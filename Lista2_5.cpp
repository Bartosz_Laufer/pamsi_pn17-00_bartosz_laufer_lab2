#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

template <typename Typ>
class deque;

template <typename Typ>
class element {
private:
	Typ wartosc;
	element *next; // wskaznik na kolejny element
	element(); // konstruktor
	friend deque<Typ>;
};

// konstruktor
template <typename Typ>
element<Typ>::element() {
	next = nullptr;
}

template <typename Typ>
class deque {
public:
	void dodajFirst(const Typ &wartosc); //funkcja dodajaca element na koniec kolejki
	void dodajLast(const Typ &wartosc);
	void wyswietl(); // funkcja wyswietlajaca kolejki
	void usunFirst(); // usuwanie pierwszego elementu z kolejki
	void usunLast(); // usuwanie ostatniego elementu z kolejki
	int rozmiar; // aktualny rozmiar kolejki
	deque(); // konstruktor
	void wypelnij(string wyraz); // wypelnia kolejke wyrazem do sprawdzenia
	Typ pierwszy(); // zwraca pierwszy element kolejki
	Typ ostatni(); // zwraca ostatni element kolejki
private:
	element<Typ> *first; // wskaznik na pierwszy element kolejki
	element<Typ> *last; // wskaznik na ostatni element kolejki
};

// konstruktor
template <typename Typ>
deque<Typ>::deque() {
	first = nullptr;
	last = nullptr;
	rozmiar = 0;
}

template <typename Typ>
Typ deque<Typ>::pierwszy()
{
	return first->wartosc; // zwraca wartosc pierwszego elementu kolejki
}

template <typename Typ>
Typ deque<Typ>::ostatni()
{
	return last->wartosc; // zwraca wartosc ostatniego elementu kolejki
}

template <typename Typ>
void deque<Typ>::wyswietl()
{
	if (rozmiar == 0) { // gdy kolejka pusta, wyswietlany jest komunikat
		cout << "Lista jest pusta!" << endl;
	}
	else {
		if (first != nullptr) {
			element<Typ> *temp = first; // wskaznik na pierwszy element kolejki
			while (temp != nullptr) {
				cout << temp->wartosc;
				temp = temp->next;  // "przelatuje" po kolei po wszystkich elementach
			}
			cout << endl;
			//cout << "Rozmiar: " << rozmiar << endl;
		}
	}
}

template <typename Typ>
void deque<Typ>::dodajFirst(const Typ &wartosc)
{
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	if (first == nullptr) { // jesli to pierwszy element kolejki, to zostaje jej poczatkiem
		first = nowy;
		last = first; // pierwszy element kolejki jest rowniez ostatnim
	}
	else {
		element<Typ> *temp = first;
		nowy->next = temp; // nowy element wskazuje na wczesniejszy pierwszy
		first = nowy; // nowy element jest teraz pierwszym
	}
	rozmiar++;
}

template <typename Typ>
void deque<Typ>::dodajLast(const Typ &wartosc)
{
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	if (first == nullptr) {
		first = nowy;
		last = first;
	}
	else {
		element<Typ> *temp = first;
		while (temp->next != nullptr) { // znajduje wskaznik na ostatni element
			temp = temp->next;
		}
		temp->next = nowy; // ostatni element wskazuje na nowy
		last = nowy; // nowo dodany element jest ostatnim
		last->next = nullptr; // nowy element nie wskazuje na nic
	}
	rozmiar++;
}

template <typename Typ>
void deque<Typ>::usunFirst()
{
	element<Typ> *temp = first;
	first = first->next; // nowy poczatek kolejki znajduje sie jeden element dalej
	if (rozmiar == 1)
		last = first;
	delete temp;
	rozmiar--;
}

template <typename Typ>
void deque<Typ>::usunLast()
{
	if (rozmiar == 1) // ostatni element jest wtedy takze pierwszym
		usunFirst(); 
	else
	{
		element<Typ> *temp = first;
		while (temp->next->next != nullptr) { // do szukania elementu poprzedzajacego usuwany
			temp = temp->next;
		}
		temp->next = nullptr; // w takim przypadku zerowany jest ostatni wskaznik
		last = temp; // cofniecie ostatniego elementu o 1
		rozmiar--;
	}
}

template <typename Typ>
void deque<Typ>::wypelnij(const string wyraz)
{
	int dlugoscWyrazu = wyraz.length();
	for (int i = 0; i < dlugoscWyrazu; i++)
		dodajLast(wyraz[i]);
}

template <typename Typ>
bool jestPal(deque<Typ> *deq)
{
	if (deq->rozmiar == 0 || deq->rozmiar == 1) // gdy warunek spelniony, to znaczy, ze sprawdzone zostaly wszystkie litery lub nie zostala sprawdzona litera srodkowa (nie wplywa na to czy slowo jest palindromem)
		return true;
	else {
		if (deq->pierwszy() == deq->ostatni()) { // sprawdzenie czy litera z pierwszej polowy wyrazu jest taka sama jak jej odpowiednik w drugiej polowie
			deq->usunFirst(); // usuniecie pierwszego wyrazu 
			deq->usunLast(); // usuniecie ostatniego wyrazu
			return jestPal(deq); // rekurencyjne wywolanie funkcji sprawdzajacej, argumenty 'lewy' i 'prawy' przesuniete o 1 w przod lub w tyl
		}
		else
			return false;
	}
}

int main()
{
	deque<char> *deq = new deque<char>;
	string wyraz; // wyraz do sprawdzenia czy jest palindromem
	cout << "Podaj wyraz do sprawdzenia: " << endl;
	cin >> wyraz;
	deq->wypelnij(wyraz);
	if (jestPal(deq) == true)
		cout << "Podany wyraz jest palindromem" << endl;
	else
		cout << "Podany wyraz nie jest palindromem" << endl;
}