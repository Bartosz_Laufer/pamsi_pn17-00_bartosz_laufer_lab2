#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

template <typename Typ>
class kolejka;

template <typename Typ>
class element {
private:
	Typ wartosc;
	element *next; // wskaznik na kolejny element
	element(); // konstruktor
	friend kolejka<Typ>;
};

// konstruktor
template <typename Typ>
element<Typ>::element() {
	next = nullptr;
}

template <typename Typ>
class kolejka {
public:
	void dodaj(const Typ &wartosc); //funkcja dodajaca element na koniec kolejki
	void wyswietl(); // funkcja wyswietlajaca liste
	void usun(); // usuwanie pierwszego elementu z kolejki
	void usunWszystkie(); // usuwanie wszystkich elementow kolejki
	int rozmiar; // aktualny rozmiar kolejki
	kolejka(); // konstruktor
private:
	element<Typ> *first; // wskaznik na pierwszy element kolejki
};

// konstruktor
template <typename Typ>
kolejka<Typ>::kolejka() {
	first = nullptr;
	rozmiar = 0;
}

template <typename Typ>
void kolejka<Typ>::dodaj(const Typ &wartosc) {
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;

	if (first == nullptr) { // jesli to pierwszy element , to zostaje jej poczatkiem
		first = nowy;
		rozmiar++;
	}
	else { // jesli jest juz jakis element na liscie to trzeba dojsc na jej koniec
		element<Typ> *temp = first;
		while (temp->next != nullptr) { // znajduje wskaznik na ostatni element
			temp = temp->next;
		}
		temp->next = nowy; // ostatni element wskazuje na nowy
		nowy->next = nullptr; // nowy element nie wskazuje na nic
		rozmiar++;
	}
	cout << "Element zostal dodany" << endl << endl;
}

template <typename Typ>
void kolejka<Typ>::wyswietl()
{
	if (rozmiar == 0) { // gdy lista pusta, wyswietlany jest komunikat
		cout << "Kolejka jest pusta!" << endl;
	}
	else {
		if (first != nullptr) {
			element<Typ> *temp = first; // wskaznik na pierwszy element listy
			while (temp != nullptr) {
				cout << temp->wartosc << " ";
				temp = temp->next;  // "przelatuje" po kolei po wszystkich elementach
			}
			cout << endl;
			//cout << "Rozmiar: " << rozmiar << endl;
		}
	}
}

template <typename Typ>
void kolejka<Typ>::usun()
{
	if (rozmiar > 0) { // sprawdzenie czy kolejka nie jest pusta
		element<Typ> *temp = first;
		first = first->next; // nowy poczatek listy znajduje sie jeden element dalej
		delete temp;
		rozmiar--;
		cout << "Element zostal usuniety" << endl << endl;
	}
	else
		cout << "Kolejka jest pusta!" << endl << endl;
}

template <typename Typ>
void kolejka<Typ>::usunWszystkie()
{
	int rozmiarTemp = rozmiar; // rozmiar bedzie sie zmiejszal z kazda operacja usuniecia, rozmiarTemp zapamieta ile razy te operacje wykonac
	for (int i = 0; i <= rozmiarTemp - 1; i++) {
		usun(); // usuwanie pierwszego elementu kolejki, az usuniety zostanie kazdy
	}
}

void wyswietlMenu()
{
	cout << "-------------MENU-------------" << endl;
	cout << "1. Dodaj element na koniec kolejki" << endl;
	cout << "2. Usun element z poczatku kolejki" << endl;
	cout << "3. Wyswietl kolejke" << endl;
	cout << "4. Usun wszystkie elementy z kolejki" << endl;
	cout << "0. Wyjdz z programu" << endl << endl;
}

int main()
{
	kolejka<string> *queue = new kolejka<string>;
	string doDodania; // liczba do dodania na koniec kolejki
	char option; // opcja wybrana przez uzytkownika

	do {
		wyswietlMenu(); //wyswietlenie menu
		option = _getch(); // pobranie znaku
		cout << endl;

		switch (option) {
		case '1': // dodawanie elementu
			cout << "Jaka liczbe chcesz dodac?" << endl;
			cin >> doDodania;
			queue->dodaj(doDodania);
			break;
		case '2': // usuwanie elementu
			queue->usun();
			break;
		case '3': // wyswietlanie kolejki
			queue->wyswietl();
			cout << endl;
			break;
		case '4': // usuwanie wszystkich elementow
			if (queue->rozmiar != 0) {
				queue->usunWszystkie();
				cout << "Wszystkie elementy zostaly usuniete" << endl << endl;
			}
			else
				cout << "Kolejka jest pusta!" << endl << endl;
			break;
		default: // gdy uzytkownik wybierze nieistniejaca opcje
			if (option != '0')
				cout << "Nie ma takiej opcji!" << endl;
			break;
		}
	} while (option != '0'); // koniec programu, gdy uzytkownik wybierze '0'
}