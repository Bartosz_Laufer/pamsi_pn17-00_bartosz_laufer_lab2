#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

template <typename Typ>
class lista;

template <typename Typ>
class element {
private:
	Typ wartosc;
	element *next; // wskaznik na kolejny element
	element(); // konstruktor
	friend lista<Typ>;
};

// konstruktor
template <typename Typ>
element<Typ>::element() {
	next = nullptr;
}

template <typename Typ>
class lista {
public:
	void dodaj(const Typ &wartosc); //funkcja dodajaca element do listy
	void wyswietl(); // funkcja wyswietlajaca liste
	void usun(int gdzieUsunac); // usuwanie pojedynczego elementu z listy
	void usunWszystkie(); // usuwanie wszystkich elementow listy
	int rozmiar; // aktualny rozmiar listy
	lista(); // konstruktor
private:
	element<Typ> *first; // wskaznik na pierwszy element listy
	element<Typ> *last; // wskaznik na ostatni element listy
};

// konstruktor
template <typename Typ>
lista<Typ>::lista() {
	first = nullptr;
	last = nullptr;
	rozmiar = 0;
}

template <typename Typ>
void lista<Typ>::wyswietl()
{
	if (rozmiar == 0) { // gdy lista pusta, wyswietlany jest komunikat
		cout << "Lista jest pusta!" << endl << endl;
	}
	else {
		if (first != nullptr) {
			element<Typ> *temp = first; // wskaznik na pierwszy element listy
			while (temp != nullptr) {
				cout << temp->wartosc << " ";
				temp = temp->next;  // "przelatuje" po kolei po wszystkich elementach
			}
			cout << endl;
			//cout << "Rozmiar: " << rozmiar << endl;
		}
	}
}

template <typename Typ>
void lista<Typ>::dodaj(const Typ &wartosc)
{
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	if (first == nullptr) { // jesli lista jest pusta
		first = nowy;
		last = first;
	}
	else {
		element<Typ> *temp = new element<Typ>;
		temp = last;
		last = nowy;
		last->next = nullptr;
		temp->next = last;
	}
	rozmiar++;
}

template <typename Typ>
void lista<Typ>::usun(int gdzieUsunac)
{
	if (rozmiar - 1 >= gdzieUsunac && gdzieUsunac >= 0) { // spradzenie, czy element ktory ma byc usuniety istnieje
		if (gdzieUsunac == 0) { // jesli do usuniecia jest pierwszy element
			element<Typ> *temp = first;
			first = first->next; // nowy poczatek listy znajduje sie jeden element dalej
			if (rozmiar == 1)
				last = first;
			delete temp;
			rozmiar--;
		}
		else if (gdzieUsunac > 0) { // jesli do usuniecia jest element inny niz pierwszy
			int prev = 1; // do szukania wskaznika na element poprzedzajacy ten usuwany
			element<Typ> *temp = first;
			while (temp != nullptr) { // do szukania elementu poprzedzajacego usuwany
				if (prev == gdzieUsunac) // gdy wskaznik jest na elemencie poprzedzajacym usuwany
					break;
				temp = temp->next;
				prev++;
			}
			if (temp->next == last) { // sprawdza czy do usuniecia jest ostatni element
				temp->next = nullptr; // w takim przypadku zerowany jest ostatni wskaznik
				last = temp; // cofniecie ostatniego elementu o 1
				rozmiar--;
			}
			else { // do usuniecia element w srodku listy
				temp->next = temp->next->next; // element n-1 wskazuje na n+1, element n zostaje usuniety
				rozmiar--;
			}
		}
		cout << "Element zostal usuniety" << endl << endl;
	}
	else {
		cout << "Podano indeks nieistniejacy na liscie!" << endl << endl;
	}
}

template <typename Typ>
void lista<Typ>::usunWszystkie()
{
	int rozmiarTemp = rozmiar; // rozmiar bedzie sie zmiejszal z kazda operacja usuniecia, rozmiarTemp zapamieta ile razy te operacje wykonac
	for (int i = 0; i <= rozmiarTemp - 1; i++) {
		usun(0); // usuwanie pierwszego elementu na liscie, az usuniety zostanie kazdy
	}
}

void wyswietlMenu()
{
	cout << "-------------MENU-------------" << endl;
	cout << "1. Dodaj element do listy" << endl;
	cout << "2. Usun element z listy" << endl;
	cout << "3. Wyswietl liste" << endl;
	cout << "4. Usun wszystkie elementy z listy" << endl;
	cout << "0. Wyjdz z programu" << endl << endl;
}

int main()
{
	lista<int> *list = new lista<int>;
	char option; // opcja wybrana przez uzytkownika
	int doDodania; // liczba do dodania na koniec listy
	int gdzie; // indeks z ktorego element ma byc usuniety

	do {
		wyswietlMenu(); //wyswietlenie menu
		option = _getch(); // pobranie znaku
		cout << endl;

		switch (option) {
		case '1': // dodawanie elementu
			cout << "Jaka liczbe chcesz dodac?" << endl;
			cin >> doDodania;
			list->dodaj(doDodania);
			break;
		case '2': // usuwanie elementu
			if (list->rozmiar != 0) {
				cout << "Z jakiego miejsca usunac liczbe? Lista ma " << list->rozmiar << " elementow" << endl;
				cin >> gdzie;
				list->usun(gdzie);
			}
			else
				cout << "Lista jest pusta!" << endl << endl;
			break;
		case '3': // wyswietlanie listy
			list->wyswietl();
			cout << endl;
			break;
		case '4': // usuwanie wszystkich elementow
			if (list->rozmiar != 0) {
				list->usunWszystkie();
				cout << "Wszystkie elementy zostaly usuniete" << endl << endl;
			}
			else
				cout << "Lista jest pusta!" << endl << endl;
			break;
		default: // gdy uzytkownik wybierze nieistniejaca opcje
			if (option != '0')
				cout << "Nie ma takiej opcji!" << endl;
			break;
		}
	} while (option != '0'); // koniec programu, gdy uzytkownik wybierze '0'
}