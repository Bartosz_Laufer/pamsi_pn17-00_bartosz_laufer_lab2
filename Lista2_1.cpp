#include <iostream>
#include <string>
#include <vector>
#include <ctime>

using namespace std;

// zmienne globalne
vector<string> dane;

// funkcja zamieniajaca dwie litery miejscami
void zamien(char *a, char *b)
{
	char temp;
	temp = *a;
	*a = *b;
	*b = temp;
}


// funkcja sprawdzajaca czy dany wyraz jest palindormem
bool jestPal(const string testStr, int lewy, int prawy)
{
	if (prawy <= lewy) // gdy warunek spelniony to znaczy, ze sprawdzone zostaly wszystkie litery lub nie zostala sprawdzona litera srodkowa (nie wplywa na to czy slowo jest palindromem)
		return true;
	else {
		if (testStr[lewy] == testStr[prawy]) { // sprawdzenie czy litera z pierwszej polowy wyrazu jest taka sama jak jej odpowiednik w drugiej polowie
			lewy++; // przesuniecie indeksu odnoszacego sie do lewej polowy wyrazu o 1 dalej
			prawy--; // przesuniecie indeksu odnoszacego sie do prawej polowy wyrazu o 1 w lewo
			return jestPal(testStr, lewy, prawy); // rekurencyjne wywolanie funkcji sprawdzajacej, argumenty 'lewy' i 'prawy' przesuniete o 1 w przod lub w tyl
		}
		else
			return false;
	}
}

// funkcja znajdujaca wszystkie permutacje danego slowa
void permutacje(string testStr, int poczatek, int koniec)
{
	if (poczatek == koniec) { // zamienianie liter miejscami doszlo do ostatniej litery
		if (jestPal(testStr, 0, testStr.length() - 1) == true) {
			dane.push_back(testStr);
			//indeks++;
		}
	}

	else {
		for (int i = poczatek; i <= koniec; i++) {
			zamien(&testStr[0 + poczatek], &testStr[0 + i]); // zamiana liter miejscami
			permutacje(testStr, poczatek + 1, koniec); // wywolanie rekurencyjnie funkcji, teraz bedzie zamieniala litery na kolejnym miejscu
			zamien(&testStr[0 + i], &testStr[0 + poczatek]); // powrot liter na wczesniejsza pozycje
		}
	}
}

// funkcja usuwajaca duplikaty z tablicy
void usunDup()
{
	for (int i = 0; i < dane.size(); i++) {
		for (int j = i + 1; j < dane.size(); j++) {
			if (dane[i] == dane[j])  {// sprawdzenie, czy dany palindrom powtarza sie w tablicy
				dane.erase(dane.begin() + j);
				j--;
			}
		}
	}
}

// funkcje wyswietlajace palindromy

/*
void wyswietlA()
{
cout << "Oto wszystkie palindromy: " << endl;
for (int i = 0; i < dane.size(); i++)
cout << dane[i] << endl;
cout << endl;
}
*/

void wyswietlB()
{
	cout << "Oto palindromy po usunieciu duplikatow: " << endl;
	for (int i = 0; i < dane.size(); i++)
		cout << dane[i] << endl;
}

int main()
{
	clock_t t1, t2;
	int length; // dlugosc wyrazu
	string testStr;
	cout << "Wprowadz wyraz: ";
	cin >> testStr;
	length = testStr.length();
	t1 = t2 = clock();
	permutacje(testStr, 0, length - 1); // dlugosc wyrazu pomniejszona o jeden, poniewaz indeksowanie rozpoczyna sie od 0
	t2 = clock();
	cout << "Czas: ";
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << "ms." << endl;
	if (dane.size() == 0)
		cout << "Z liter podanego wyrazu nie da sie utworzyc zadnego palindromu!" << endl;
	else {
		//wyswietlA(); // wyswietlenie wszystkich palindromow
		usunDup();
		wyswietlB(); // wyswietlenie palindromow po usunieciu duplikatow
		cout << "Jest " << dane.size() << " palindromow" << endl;
	}
}